# Makefile

# Options & compilateur
CC = gcc
OPTIONS = -w -Wall -pedantic -std=c11 `pkg-config --cflags MLV` `pkg-config --libs-only-other --libs-only-L MLV`
OPTIONS_b = `pkg-config --libs-only-l MLV`
REP = ./includes/

# Les makes
main : $(REP)main.c $(REP)plateau.o $(REP)pion.o $(REP)affichage.o $(REP)clic.o $(REP)jeu1vs1.o $(REP)jeu1vsia.o $(REP)jeu1vsia1.o
	$(CC) $(OPTIONS)  $(REP)main.c $(REP)plateau.o $(REP)pion.o $(REP)affichage.o $(REP)clic.o  $(REP)jeu1vs1.o  $(REP)jeu1vsia.o $(REP)jeu1vsia1.o $(OPTIONS_b) -o main

plateau.o : $(REP)plateau.c $(REP)plateau.h
	$(CC) $(OPTIONS) $(OPTIONS_b) -c $(REP)plateau.c -o $(REP)plateau.o

pion.o : $(REP)pion.c $(REP)pion.h
	$(CC) $(OPTIONS) $(OPTIONS_b) -c $(REP)pion.c -o $(REP)pion.o

affichage.o: $(REP)affichage.c $(REP)affichage.h
	$(CC) $(OPTIONS) $(OPTIONS_b) -c $(REP)affichage.c -o $(REP)affichage.o

clic.o : $(REP)clic.c $(REP)clic.h
	$(CC) $(OPTIONS) $(OPTIONS_b) -c $(REP)clic.c -o $(REP)clic.o

jeu1vs1.o : $(REP)jeu1vs1.c $(REP)jeu1vs1.h
	$(CC) $(OPTIONS) $(OPTIONS_b) -c $(REP)jeu1vs1.c -o $(REP)jeu1vs1.o

jeu1vsia.o : $(REP)jeu1vsia.c $(REP)jeu1vs1.h
	$(CC) $(OPTIONS) $(OPTIONS_b) -c $(REP)jeu1vsia.c -o $(REP)jeu1vsia.o

jeu1vsia1.o : $(REP)jeu1vsia1.c $(REP)jeu1vsia1.h
	$(CC) $(OPTIONS) $(OPTIONS_b) -c $(REP)jeu1vsia1.c -o $(REP)jeu1vsia1.o

# Le clean
clean :
	rm -f main $(REP)*.o $(REP)#*# ./#*#
