#include "jeu1vs1.h"
#include "time.h"

int jeu1vsia (){
  int pion_J1 = 0, pion_J2 = 0, joueur = 1, joueur_b = 2, taille, ia, x, y;
  matrice plateau ;
  coordonnees clic, *c_pion_vert = (coordonnees*) malloc (sizeof(coordonnees));  
  srand(time(NULL));
  MLV_Image *plateau_bois, *plateau_fond;

  MLV_create_window("otellox", "otellox", F_PLATEAU + BORDURE + 357, F_PLATEAU + 2 * BORDURE + 6);
  plateau_bois = MLV_load_image("./includes/images/plateau.png");
  plateau_fond = MLV_load_image("./includes/images/fond.png");

  mettre_zero_matrice(plateau);
  initialiser_matrice(plateau);
  afficher_matrice_terminale(plateau);
  afficher_hud(plateau_bois);
  afficher_plateau(plateau_fond);
  afficher_pion(plateau);
  MLV_change_frame_rate(30);
  while ( pion_J1 < 2 || pion_J2 < 2 ){
      MLV_clear_window(MLV_rgba(0, 0, 0, 255));
      fprintf(stdout, "\n");
      afficher_matrice_terminale(plateau);
      fprintf(stdout, "J1:%d J2:%d\n", pion_J1, pion_J2); 
      afficher_hud(plateau_bois);
      afficher_plateau(plateau_fond);
      afficher_pion(plateau);
      if(pion_vert(plateau) > 0){
	if (joueur == 1){
	  do{
	    clic_souris(&clic); 
	  }
	  while(peut_poser(&clic, plateau, &joueur) == 0);
	}
	else {
	  taille = pion_vert(plateau);
	  c_pion_vert=coordonne_pion_vert(plateau, c_pion_vert, taille);
	  for (ia = 0 ; ia < taille  ; ia++){
	    printf("TAB[%d] : x = %d y = %d\n",ia,c_pion_vert[ia].x,c_pion_vert[ia].y);
	  }
	  ia = rand()%taille;;
	  clic.x = c_pion_vert[ia].x;
	  clic.y = c_pion_vert[ia].y;
	  plateau[clic.x][clic.y] = 2;
	  printf(" x : %d\n y : %d\n",clic.x,clic.y);
	}
      }
      compte_pion(plateau,&pion_J1,&pion_J2);
      changement_de_joueur(&joueur, &joueur_b);
  }
  do{
    MLV_clear_window(MLV_rgba(0, 0, 0, 255));
    check_pion_autour(plateau, &joueur, &joueur_b);
    fprintf(stdout, "\n");
    afficher_matrice_terminale(plateau);
    fprintf(stdout, "J1:%d J2:%d\n", pion_J1, pion_J2); 
    afficher_hud(plateau_bois);
    afficher_plateau(plateau_fond);
    if ( joueur == 1 ){
      afficher_pion(plateau);
    }
    if(pion_vert(plateau) > 0){
      if (joueur == 1){      
	do{
	  clic_souris(&clic);
	}
	while(peut_poser(&clic, plateau, &joueur) == 0);
      }
      else {
	taille = pion_vert(plateau);
	c_pion_vert=coordonne_pion_vert(plateau, c_pion_vert,taille);
	ia = rand()%taille;;
	clic.x = c_pion_vert[ia].x;
	clic.y = c_pion_vert[ia].y;
	plateau[clic.x][clic.y] = 2;
	printf(" x : %d\n y : %d\n",clic.x,clic.y);
      }
      fprintf(stdout, "x:%d y:%d\nj1:%d j2:%d\n", clic.x, clic.y, joueur, joueur_b);
    }
    check_pion_autour_retourner(plateau, &joueur, &joueur_b, &clic);
    compte_pion(plateau,&pion_J1,&pion_J2);
    if (joueur == 1){
      afficher_pion(plateau);
    }
    parcourt_plateau(plateau);
    changement_de_joueur(&joueur, &joueur_b);
    MLV_delay_according_to_frame_rate();
  }
  while(pion_J1 + pion_J2 < T_PLAT*T_PLAT);
  afficher_pion(plateau);
  MLV_wait_seconds(1);
  fprintf(stdout, "\n");
  afficher_matrice_terminale(plateau);
  fprintf(stdout, "\n"); 
  MLV_free_image(plateau_fond);
  MLV_free_image(plateau_bois);
  MLV_free_window();
  exit(0);
}
