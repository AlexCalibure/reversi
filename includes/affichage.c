#ifndef AFFICHAGE_C
#define AFFICHAGE_C

/* Les includes */
#include "affichage.h"

/* Les fonctions */
void afficher_plateau(MLV_Image *fond){
   int i;
   MLV_draw_image(fond, BORDURE + 3, BORDURE + 3);
   for(i = 0 ; i <= T_PLAT ; i++){
      MLV_draw_line(i*CASE + BORDURE + 3, BORDURE + 3, i*CASE + BORDURE + 3, CASE*T_PLAT + BORDURE + 3, MLV_rgba(0, 0, 0, 255));
      MLV_draw_line(BORDURE + 3, i*CASE + BORDURE + 3, CASE*T_PLAT + BORDURE + 3, i*CASE + BORDURE + 3, MLV_rgba(0, 0, 0, 255));
   }
}

void afficher_pion(matrice m){
   MLV_Color couleur, couleur_tour;
   int i, j;
   for(i = 1 ; i <= T_PLAT ; i++)
      for(j = 1 ; j <= T_PLAT ; j++)
         if(m[i][j] != 0){
            if(m[i][j] == 1){
               couleur = MLV_rgba(0, 0, 0, 255);
               couleur_tour = MLV_rgba(0, 0, 0, 255);
            }
	    if(m[i][j] == 2){
	      couleur = MLV_rgba(255, 255, 255, 255);
	      couleur_tour = MLV_rgba(0, 0, 0, 255);
            }
            if(m[i][j] == 3){
	      couleur = MLV_rgba(29, 228, 42, 100);
	      couleur_tour = MLV_rgba(50, 201, 20, 200);
            }
	    MLV_draw_filled_circle((i - 1)*CASE + CASE/2 + BORDURE + 3, (j - 1)*CASE + CASE/2 + BORDURE + 3, PION, couleur);
            MLV_draw_circle((i - 1)*CASE + CASE/2 + BORDURE + 3, (j - 1)*CASE + CASE/2 + BORDURE + 3, PION, couleur_tour);
         }
   MLV_actualise_window();
}


void afficher_hud(MLV_Image *bois){
   MLV_draw_image(bois, 3, 3);
   MLV_draw_image(bois, F_PLATEAU + 2 * BORDURE + 6, 3);
   MLV_draw_filled_rectangle(0, 0, F_PLATEAU + BORDURE + 357, 3, MLV_rgba(255, 255, 255, 255));
   MLV_draw_filled_rectangle(0, F_PLATEAU + 2 * BORDURE + 3, F_PLATEAU + BORDURE + 357, F_PLATEAU + 2 * BORDURE + 6, MLV_rgba(255, 255, 255, 255));
   MLV_draw_filled_rectangle(0, 0, 3, F_PLATEAU + 2 * BORDURE + 6, MLV_rgba(255, 255, 255, 255));
   MLV_draw_filled_rectangle(F_PLATEAU + BORDURE + 354, 0, F_PLATEAU + BORDURE + 357, F_PLATEAU + 2 * BORDURE + 6, MLV_rgba(255, 255, 255, 255));
   //MLV_draw_filled_rectangle(F_PLATEAU + 2 * BORDURE + , MLV_rgba(200, 20, 20, 255));
}

#endif
