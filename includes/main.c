#include "jeu1vs1.h"
#include "jeu1vsia1.h"
/* Le main */
int main(int argc, char*argv[]){
  if( argc != 2 ){
    printf ("Usage : ./main <mode>\n");
    exit(-1);
  }
  if (atoi(argv[1]) > 1){
      printf("Usage : ./main <mode>\n");
      exit(-1);
  }
  if ( atoi(argv[1]) == 1 ){
    jeu1vs1();
  }
  else if ( atoi(argv[1]) == 0 ){
    jeu1vsia1();
  }
  exit(0);
}
