#ifndef AFFICHAGE_H
#define AFFICHAGE_H

/* Les includes */
#include <MLV/MLV_all.h>
#include "plateau.h"

/* Les defines */
#define CASE 74
#define F_PLATEAU T_PLAT * CASE
#define PION CASE/2 - 3
#define BORDURE 20

/* Les typedef */

/* Les prototypes */
//Affiche le plateau de jeu
void afficher_plateau(MLV_Image *fond);
//Affiche les pions sur le plateau de jeu
void afficher_pion(matrice m);
//Affiche l'environnement de jeu
void afficher_hud(MLV_Image *bois);
#endif
