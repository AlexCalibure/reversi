#ifndef PLATEAU_H
#define PLATEAU_H

/* Les includes  */
#include <stdlib.h>
#include <stdio.h>

/* Les defines */
#define T_PLAT 8

/* Les typedef */
typedef int matrice[T_PLAT+2][T_PLAT+2];

/* Les prototypes */
//Remplie la matrice de zero
void mettre_zero_matrice(matrice m);
//Affiche la matrice
void afficher_matrice_terminale(matrice m);
//Initialise la matrice avec les zones où les joueurs pourront poser leurs pions
void initialiser_matrice(matrice m);
//recopie la matrice p dans la matric m
void recopie_matrice ( matrice p, matrice m);
//compare 2 matrice pour voir si elles sont identiques
int compare_matrice ( matrice a, matrice b);
#endif
