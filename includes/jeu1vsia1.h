#include "jeu1vs1.h"

typedef struct arbre{
  matrice noeud;
  struct arbre *fils;
}arbres;

/*arbres arbre_vide ();*/
//creer un arbre
arbres creer_arbre (matrice p, int joueur_b);
//creer un arbre a 2 niveau
arbres creer_arbre_niv2 (matrice p, int joueur, int joueur_b);
//joue un coup permettant d'avoir le plus de pion
void meilleur_choix ( arbres a, matrice p );
