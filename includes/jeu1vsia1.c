#include "jeu1vsia1.h"
#include "time.h"

arbres creer_arbre (matrice p, int joueur_b){
  arbres a,fils ;
  matrice m;
  int i, j, joueur, pion_vert_pose = 0, f = 0;
  coordonnees c_pion_pose;
  if ( joueur_b  == 1 ){
    joueur = 2;
  }
  else {
    joueur = 1;
  }
  recopie_matrice(p,a.noeud);
  parcourt_plateau(a.noeud);
  check_pion_autour(a.noeud, &joueur, &joueur_b);//recopie l'etat de la matrice du plateau dans la racine de l'arbre en enlevant tous les pions vert qui etaient initialement dessus puis ajoute les nouveau pions vert de la configuration du plateau
  recopie_matrice(a.noeud,m); //recopie la matrice de la racine afin de pouvoir la modifier sans alterer celle dans l'arbre
  int nb_fils, nb_pion_vert;
  nb_fils = nb_pion_vert = pion_vert(a.noeud);
  a.fils = ( arbres*) malloc (pion_vert(a.noeud) * sizeof(arbres));
  while ( nb_pion_vert > 0 ){
    for ( i = 0; i < T_PLAT + 2; i++){
      for ( j = 0; j < T_PLAT + 2; j++){
	if ( m[i][j] == 3 && pion_vert_pose == 0 ){ //On recopie le plateau de jeu en plaçant un pion du joueur a l'emplacement des coup possible. Chaque coup correspond a un fils. Pion vert pose indique si on a pose un pion ou non dans la nouvelle matrice
	  m[i][j] = 0;
	  a.fils[f].noeud[i][j] = joueur_b;
	  c_pion_pose.x=i;
	  c_pion_pose.y=j;//On recupere les coordonnees du pion posé.
	  pion_vert_pose = 1;
	}
	else {
	  if ( m[i][j] == 3 ){
	    a.fils[f].noeud[i][j] = 0;
	  }
	  else{
	    a.fils[f].noeud[i][j] = m[i][j];
	  }
	}
      }
    }
    check_pion_autour_retourner(a.fils[f].noeud, &joueur, &joueur_b, &c_pion_pose);
    check_pion_autour(a.fils[f].noeud,&joueur_b,&joueur);//On retourne les pions a retourner dans la nouvelle matrice et on affiche les coups possible pour le prochain joueur.
    nb_pion_vert--;
    f++;
    pion_vert_pose = 0;
  }
  return a;
}

arbres creer_arbre_niv2 (matrice p, int joueur, int joueur_b){
  arbres a = creer_arbre(p,joueur_b);
  int nb_fils = pion_vert(a.noeud), i,j, nb_petit_fils;
  for ( i = 0; i < nb_fils; i++){
    a.fils[i]=creer_arbre(a.fils[i].noeud,joueur);//A chaque fils trouvé on lui calcul également ses fils.
    nb_petit_fils = pion_vert(a.fils[i].noeud);
    for (j = 0; j < nb_petit_fils; j++){
      a.fils[i].fils[j]=creer_arbre(a.fils[i].fils[j].noeud,joueur_b);
    }
  }
  return a;
}

void meilleur_choix ( arbres a, matrice p ){
  int nb_fils = pion_vert(a.noeud),i=0, j, nb_petit_fils, pion_J1 = 0, pion_J2 = 0,max_pionJ2 = 0, choix1, choix2;
  while ( compare_matrice(a.fils[i].noeud,p) != 0 ){// On regarde quel coup le joueur a jouer parmis la liste des fils present dans l'arbre
    i++;
    printf("i :%d\n",i);
  }
  afficher_matrice_terminale(a.fils[i].noeud);
  nb_petit_fils = pion_vert(a.fils[i].noeud);
  for( j = 0; j < nb_petit_fils; j++){ //Parmis les fils present dans l'arbre on regarde quel coup permet de recuperer le plus de pions.
    compte_pion(a.fils[i].fils[j].noeud,&pion_J1,&pion_J2);
    if ( pion_J2 > max_pionJ2 ){
      max_pionJ2 = pion_J2;
      choix1 = i;
      choix2 = j;
    }
  }
  recopie_matrice(a.fils[choix1].fils[choix2].noeud,p);  //Joue le coup qui permet de recuperer le plus de pion
}




int jeu1vsia1 (){
  int pion_J1 = 0, pion_J2 = 0, joueur = 1, joueur_b = 2, taille, ia, x, y;
  matrice plateau ;
  coordonnees clic, *c_pion_vert = (coordonnees*) malloc (sizeof(coordonnees));
  arbres arbre_ia;
  srand(time(NULL));
  MLV_Image *plateau_bois, *plateau_fond;

  MLV_create_window("otellox", "otellox", F_PLATEAU + BORDURE + 357, F_PLATEAU + 2 * BORDURE + 6);
  plateau_bois = MLV_load_image("./includes/images/plateau.png");
  plateau_fond = MLV_load_image("./includes/images/fond.png");

  mettre_zero_matrice(plateau);
  initialiser_matrice(plateau);//Initialise une matrice correspondant au plateau
  MLV_change_frame_rate(30);
  while ( pion_J1 < 2 || pion_J2 < 2 ){
      MLV_clear_window(MLV_rgba(0, 0, 0, 255));
      fprintf(stdout, "\n");
      afficher_matrice_terminale(plateau);
      fprintf(stdout, "J1:%d J2:%d\n", pion_J1, pion_J2); 
      afficher_hud(plateau_bois);
      afficher_plateau(plateau_fond);    
      afficher_pion(plateau);// Affiche le plateau et sa matrice correspondant
      if(pion_vert(plateau) > 0){
	if (joueur == 1){
	  do{
	    clic_souris(&clic); 
	  }
	  while(peut_poser(&clic, plateau, &joueur) == 0);//Demande a l'utilisateur de poser un pion en clicant sur un pion vert.
	}
	else {
	  taille = pion_vert(plateau);
	  c_pion_vert=coordonne_pion_vert(plateau, c_pion_vert, taille);//Recupere les coordonnees de tous les pions vert et les mets dans un tableau.
	  ia = rand()%taille;;
	  clic.x = c_pion_vert[ia].x;
	  clic.y = c_pion_vert[ia].y;
	  plateau[clic.x][clic.y] = 2;//Choisi une position de départ aléatoire parmis les coups disponible
	  printf(" x : %d\n y : %d\n",clic.x,clic.y);
	}
      }
      compte_pion(plateau,&pion_J1,&pion_J2);
      changement_de_joueur(&joueur, &joueur_b);
  }
  do{
    MLV_clear_window(MLV_rgba(0, 0, 0, 255));
    check_pion_autour(plateau, &joueur, &joueur_b);
    fprintf(stdout, "\n");
    afficher_matrice_terminale(plateau);
    fprintf(stdout, "J1:%d J2:%d\n", pion_J1, pion_J2); 
    afficher_hud(plateau_bois);
    afficher_plateau(plateau_fond);//Afiche le plateau et sa matrice avec le nombre de pion
    if(pion_vert(plateau) > 0){ //Verifie qu'un coup est possible pour le joueur en cour.
      if (joueur == 1){      
	  printf ("ARBRE : \n\n");
	  arbre_ia = creer_arbre_niv2(plateau,joueur,joueur_b); //creer ou met a jour l'arbre avec la configuration du plateau actuel.
	  afficher_pion(plateau);
	do{
	  clic_souris(&clic);//Le joueur pose son pion 
	}
	while(peut_poser(&clic, plateau, &joueur) == 0);
	afficher_matrice_terminale(plateau);
	printf("\n\n");
      }
      parcourt_plateau(plateau);
      check_pion_autour_retourner(plateau, &joueur, &joueur_b, &clic);//Retourne les pions a retourner suite au coup poser par le joueur
      check_pion_autour(plateau,&joueur_b,&joueur);
      meilleur_choix(arbre_ia,plateau); //l'ia joue le coup lui permettant de recuperer le plus de pion.
      afficher_matrice_terminale(plateau);
      fprintf(stdout, "x:%d y:%d\nj1:%d j2:%d\n", clic.x, clic.y, joueur, joueur_b);
      compte_pion(plateau,&pion_J1,&pion_J2);
      if (joueur == 1){
	afficher_pion(plateau);
      }
      parcourt_plateau(plateau);//Nettoie le plateau
      free(arbre_ia.fils);
      MLV_delay_according_to_frame_rate();
    }
    else { //Si le premier joueur ne peut pas poser de pion on regarde pour le prochain jouer s'il peut, s''il peut pas le jeu se fini.
      changement_de_joueur(&joueur,&joueur_b);
      check_pion_autour(plateau, &joueur, &joueur_b); 
	if(pion_vert(plateau) > 0){
	  break;
	}
	else{
	  changement_de_joueur(&joueur,&joueur_b);
	}
    }
  }
  while(pion_J1 + pion_J2 < T_PLAT*T_PLAT);
  afficher_pion(plateau);
  MLV_wait_seconds(1);
  fprintf(stdout, "\n");
  afficher_matrice_terminale(plateau);
  fprintf(stdout, "\n");
  if (pion_J1 > pion_J2){
    printf("J1 GAGNE !\n");
  }
  else {
    printf ("J2 GAGNE !\n");
  } //Affiche le gagnant.
  MLV_free_image(plateau_fond);
  MLV_free_image(plateau_bois);
  MLV_free_window();
  exit(0);
}
