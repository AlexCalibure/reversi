#ifndef PLATEAU_C
#define PLATEAU_C

/* Les includes */
#include "plateau.h"

/* Les fonctions */
void mettre_zero_matrice(matrice m){
   int i, j;
   for(i = 1 ; i < T_PLAT + 1 ; i++)
      for(j = 1 ; j < T_PLAT + 1 ; j++)
         m[i][j] = 0;
}

void afficher_matrice_terminale(matrice m){
   int i, j;
   printf("\n");
   for(i = 0 ; i < T_PLAT + 2 ; i++){
    for(j = 0 ; j < T_PLAT + 2 ; j++)
      fprintf(stdout, "%d ", m[j][i]);
    fprintf(stdout, "\n");
   }
}

void initialiser_matrice(matrice m){
   int i;
   m[(T_PLAT + 2)/2 - 1][(T_PLAT + 2)/2 - 1] = 3;
   m[(T_PLAT + 2)/2][(T_PLAT + 2)/2 - 1] = 3;
   m[(T_PLAT + 2)/2][(T_PLAT + 2)/2] = 3;
   m[(T_PLAT + 2)/2 - 1][(T_PLAT + 2)/2] = 3;
   for(i = 0 ; i <= T_PLAT + 1 ; i++){
      m[0][i] = 5;
      m[i][0] = 5;
      m[T_PLAT + 1][i] = 5;
      m[i][T_PLAT + 1] = 5;
   }
}

void recopie_matrice ( matrice p, matrice m){
  int i, j;
      for ( i = 0; i < T_PLAT + 2; i++){
	for ( j = 0; j < T_PLAT + 2; j++){
	  m[i][j]=p[i][j];
	}
      }
}

int compare_matrice ( matrice a, matrice b){
  int i,j;
  //afficher_matrice_terminale(a);
  //afficher_matrice_terminale(b);
  for ( i = 0; i < T_PLAT + 2; i++){
    for ( j = 0; j < T_PLAT + 2; j++){
      if ( a[i][j] != b[i][j] ){
	return 1;
      }
    }
  }
  return 0;
}
#endif
